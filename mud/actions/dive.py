# -*- coding: utf-8 -*-
# Copyright (C) 2016 Valentin Régnier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import DiveEvent

class DiveAction(Action2):
    EVENT = DiveEvent
    ACTION = "breath"
    RESOLVE_OBJECT = "resolve_for_use"
