# -*- coding: utf-8 -*-
# Copyright (C) 2016 Valentin Régnier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import DiveEvent

class DiveEffect(Effect2):
    EVENT = DiveEvent
