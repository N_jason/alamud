# -*- coding: utf-8 -*-
# Copyright (C) 2016 Valentin Régnier, IUT d'Orléans
#==============================================================================

from .event import Event2

class DiveEvent(Event2):
    NAME = "breath"

    def perform(self):
        if not self.object.has_prop("aquatic"):
            self.fail()
            return self.inform("breath.failed")
        self.inform("breath")
